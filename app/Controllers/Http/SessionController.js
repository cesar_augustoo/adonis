"use strict";

class SessionController {
  async store({ request, response, auth }) {
    //verificando quem está fazendo o login
    const { email, password } = request.all();

    //gerando o token
    const token = await auth.attempt(email, password);

    return token;
  }
}

module.exports = SessionController;
